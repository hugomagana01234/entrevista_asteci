import java.util.Scanner;
import java.util.Random;

public class Juego{

    public static void main(String [] args)
    {
        Scanner in = new Scanner(System.in);
        Random random = new Random();
        int numIntentos = 0;
        int entrada;    
        int numAleatoreo = random.nextInt(100+1)+1;
        while(numIntentos != 5){
            System.out.println("Ingrece un número: ");
            entrada = in.nextInt();    
            if( numAleatoreo == entrada ){
                System.out.println("Haz acertado"); 
                break;  
            } else if(numAleatoreo > entrada){
                System.out.println("El número ingresado es menor"); 
                numIntentos++;
            }else{
                System.out.println("El número ingresado es mayor"); 
                numIntentos++;
            }
        }
        System.out.println("El número que no adivinaste es: "+numAleatoreo); 
    
        
        
    }
}