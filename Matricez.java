import java.util.*;
public class Matricez{

    public static void main(String [] args)
    {

        int [][] A = {{2,0,1},{3,0,0},{5,1,1}};
        int [][] B = {{1,0,1},{1,2,1},{1,1,0}};

        int [][] C = new int [3][3];

        //Multiplicar
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                int suma = 0;
                for (int k=0; k<3; k++){
                    suma += A[j][k] * B[k][i];
                }
                C[j][i] = suma;
            }
        }

        //Mostrar
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                System.out.print(C[i][j] + " ");
            } 
            System.out.println("");
        }

        
    }
}