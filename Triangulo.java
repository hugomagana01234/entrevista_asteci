import java.util.Scanner;
public class Triangulo{

    public static void main(String [] args)
    {
        System.out.println("Ingrece el valor de n: ");
        Scanner in = new Scanner(System.in);
        int n =  in.nextInt();
    
        int contador = 0;
        for (int i=0; i<n; i++){
            for (int j=0; j<i+1; j++){
                contador ++;
                System.out.print(contador+" ");
            }
            System.out.println("");
        }

        
    }
}
